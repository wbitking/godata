require 'test_helper'

class UseragendasControllerTest < ActionController::TestCase
  setup do
    @useragenda = useragendas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:useragendas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create useragenda" do
    assert_difference('Useragenda.count') do
      post :create, useragenda: { agendaFlag: @useragenda.agendaFlag, agenda_id: @useragenda.agenda_id, sponsorFlag: @useragenda.sponsorFlag, user_id: @useragenda.user_id }
    end

    assert_redirected_to useragenda_path(assigns(:useragenda))
  end

  test "should show useragenda" do
    get :show, id: @useragenda
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @useragenda
    assert_response :success
  end

  test "should update useragenda" do
    patch :update, id: @useragenda, useragenda: { agendaFlag: @useragenda.agendaFlag, agenda_id: @useragenda.agenda_id, sponsorFlag: @useragenda.sponsorFlag, user_id: @useragenda.user_id }
    assert_redirected_to useragenda_path(assigns(:useragenda))
  end

  test "should destroy useragenda" do
    assert_difference('Useragenda.count', -1) do
      delete :destroy, id: @useragenda
    end

    assert_redirected_to useragendas_path
  end
end
