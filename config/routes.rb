Rails.application.routes.draw do
  resources :reps
  resources :pictures
  resources :images
  resources :useragendas, only: [:attend]

  resources :mobiles do
    collection do
      post 'create_code'
      post 'verify'
    end
  end

  #devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions'}
  devise_for :users, controllers: { registrations: 'users/registrations'}
  devise_scope :user do
    get 'agendas/users/:id', to: 'users/registrations#profile'
    get '/home', to: 'users/registrations#home'
    get '/album', to: 'users/registrations#album'
    get '/info', to: 'users/registrations#info'
    get '/attends', to: 'users/registrations#attends'
    get '/pubs', to: 'users/registrations#pubs'
    get '/settings', to: 'users/registrations#settings'
    get '/info/nick', to: 'users/registrations#nick'
  end

  resource :user, only: [:edit] do
    collection do
      post 'create_code'
      get 'forget_password'
      get 'reset_password'
      patch 'update_password'
      get 'verify_code'
      post 'code_verification'
      post 'verify_phone'
    end
  end

  root 'agendas#index'

  post 'agendas/attend/:id', to: 'useragendas#attend'
  get 'useragendas/select/:id', to: 'useragendas#select'

  resources :agendas do
    resources :messages, except: [:show, :index]
  end

  resources :messages do
    resources :reps, except: [:show, :index]
  end

  resources :zones do  
    resources :agendas
  end

  resources :categories do  
    resources :agendas
  end

  get 'setloc' => "amap#setloc"

  get 'wechat' => "wechats#auth_wechat"
  post 'wechat' => "wechats#process_request"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

namespace :api do
  namespace :v1 do
    resources :trainers, only: [:index, :create, :show, :update, :destroy]
    get 'uptoken_gen/index'
  end
end

end
