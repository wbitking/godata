# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Zone.create(name: '静安')
Zone.create(name: '徐汇')
Zone.create(name: '黄浦')
Zone.create(name: '杨浦')
Zone.create(name: '普陀')
Zone.create(name: '浦东')
Zone.create(name: '宝山')
Zone.create(name: '虹口')
Zone.create(name: '闵行')
Zone.create(name: '松江')
Zone.create(name: '青浦')
Zone.create(name: '奉贤')

Category.create(name: '运动')
Category.create(name: '娱乐')
Category.create(name: '饭局')
Category.create(name: '旅游')

Cost.create(name: '无费用')
Cost.create(name: 'AA')
Cost.create(name: '发布人请客')
Cost.create(name: '应约人请客')

Gender.create(name: '男')
Gender.create(name: '女')

Carrer.create(name: '工程师')
Carrer.create(name: '销售')
Carrer.create(name: '艺术家')
Carrer.create(name: '财务')
Carrer.create(name: '金融')
Carrer.create(name: '教育')

Age.create(name: '18岁以下')
Age.create(name: '18～22岁')
Age.create(name: '23～26岁')
Age.create(name: '27～35岁')
Age.create(name: '35～45岁')
Age.create(name: '45岁以上')

trainers = Trainer.create([
                     {
                       email: 'test-trainer-00@mail.com',
                       name: 'test-trainer-00',
                       activated: DateTime.now,
                       admin: false
                     },
                     {
                       email: 'test-trainer-01@mail.com',
                       name: 'test-trainer-01',
                       activated: DateTime.now,
                       admin: false
                     }
                    ])