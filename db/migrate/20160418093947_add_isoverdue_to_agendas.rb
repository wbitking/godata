class AddIsoverdueToAgendas < ActiveRecord::Migration
  def change
  	add_column :agendas, :isoverdue, :integer
  end
end
