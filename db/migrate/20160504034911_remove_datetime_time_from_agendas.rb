class RemoveDatetimeTimeFromAgendas < ActiveRecord::Migration
  def change
    remove_column :agendas, :time
    remove_column :agendas, :datatime
  end
end
