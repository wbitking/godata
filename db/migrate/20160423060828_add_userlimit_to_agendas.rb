class AddUserlimitToAgendas < ActiveRecord::Migration
  def change
  	add_column :agendas, :userlimit, :integer
  	add_column :agendas, :isfull, :integer
  end
end
