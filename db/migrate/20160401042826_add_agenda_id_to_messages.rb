class AddAgendaIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :agenda_id, :integer
  end
end
