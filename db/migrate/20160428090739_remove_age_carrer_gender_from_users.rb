class RemoveAgeCarrerGenderFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :age
    remove_column :users, :carrer
    remove_column :users, :gender
  end
end
