class CreateMobiles < ActiveRecord::Migration
  def change
    create_table :mobiles do |t|
      t.string :number
      t.string :verification_code
      t.integer :is_verified

      t.timestamps null: false
    end
  end
end
