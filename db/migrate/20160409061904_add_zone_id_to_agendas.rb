class AddZoneIdToAgendas < ActiveRecord::Migration
  def change
  	add_column :agendas, :zone_id, :integer
  end
end
