class AddLngLatToAgendas < ActiveRecord::Migration
  def change
  	add_column :agendas, :lng, :float
  	add_column :agendas, :lat, :float
  end
end
