class RemoveFieldAgendaflagSponsorflagFromUseragendas < ActiveRecord::Migration
  def change
    remove_column :useragendas, :sponsorFlag
    remove_column :useragendas, :agendaFlag
  end
end
