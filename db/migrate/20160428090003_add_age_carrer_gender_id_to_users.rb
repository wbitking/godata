class AddAgeCarrerGenderIdToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :age_id, :integer
  	add_column :users, :carrer_id, :integer
  	add_column :users, :gender_id, :integer
  end
end