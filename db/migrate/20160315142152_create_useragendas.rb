class CreateUseragendas < ActiveRecord::Migration
  def change
    create_table :useragendas do |t|
      t.integer :user_id
      t.integer :agenda_id
      t.integer :agendaFlag
      t.integer :sponsorFlag

      t.timestamps null: false
    end
  end
end
