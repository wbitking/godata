class CreateAgendas < ActiveRecord::Migration
  def change
    create_table :agendas do |t|
      t.string :name
      t.string :description
      t.string :province
      t.string :city
      t.string :address
      t.string :datatime

      t.timestamps null: false
    end
  end
end
