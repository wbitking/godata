class AddTimeDatetimeToAgendas < ActiveRecord::Migration
  def change
  	add_column :agendas, :time, :time
  	add_column :agendas, :datatime, :date
  end
end
