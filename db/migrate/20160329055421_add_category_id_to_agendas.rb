class AddCategoryIdToAgendas < ActiveRecord::Migration
  def change
    add_column :agendas, :category_id, :integer
  end
end
