# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@IsPC = ->
  userAgentInfo = navigator.userAgent
  Agents = [
    'Android'
    'iPhone'
    'SymbianOS'
    'Windows Phone'
    'iPad'
    'iPod'
  ]
  flag = true
  v = 0
  while v < Agents.length
    if userAgentInfo.indexOf(Agents[v]) > 0
      flag = false
      break
    v++
  flag

@changeUploadButton = (element) -> # element为$('#images_')
  $input = element
  $label = $input.siblings().first()
  labelVal = $label.html()
  $input.on 'change', (e) ->
    fileName = ''
    if $input[0].files and $input[0].files.length > 1
      fileName = ($input.attr('data-multiple-caption') or '').replace('{count}', $input[0].files.length)
    else if e.target.value
      fileName = e.target.value.split('\\').pop()
    if fileName
      $label.html fileName
    else
      $label.html labelVal
    return
  # Firefox bug fix
  $input.on('focus', ->
    $input.addClass 'has-focus'
    return
  ).on 'blur', ->
    $input.removeClass 'has-focus'
    return

@filePreview = (input) ->
  if input.files and input.files[0]
    reader = new FileReader

    reader.onload = (e) ->
      $('.img-preview > img').remove()
      $('.img-preview').append '<img src="' + e.target.result + '" width="50" height="50"/>'
      return

    reader.readAsDataURL input.files[0]
  return

$ ->
  $('.datepicker').focus ->
    $('.datepicker').pickadate
      selectMonths: true
      selectYears: 15
  $('select').material_select()
  $('.button-collapse').sideNav
    menuWidth: 180
    edge: 'left'
    closeOnClick: true
  $('.parallax').parallax()
  $('.dropdown-button').dropdown
    inDuration: 300
    outDuration: 225
    constrain_width: true
    hover: false
    gutter: 2
    belowOrigin: true
    alignment: 'left'
  $('.slider').slider()
  $('.carousel').carousel()
  $('.modal-trigger').leanModal
    dismissible: true
    opacity: .5
    in_duration: 300
    out_duration: 200
    starting_top: '4%'
    ending_top: '10%'

  if !IsPC()
    $('input').focus ->
      $('.header-wrp').css 'position', 'absolute'
      $('#tabs-fixed').css 'position', 'absolute'
      $('#footer-index').css 'display', 'none'
      return
    $('input').blur ->
      $('.header-wrp').css 'position', 'fixed'
      $('#tabs-fixed').css 'position', 'fixed'
      $('#footer-index').css 'display', 'block'
      return
    $('textarea').focus ->
      $('.header-wrp').css 'position', 'absolute'
      $('#tabs-fixed').css 'position', 'absolute'
      $('#footer-index').css 'display', 'none'
      return
    $('textarea').blur ->
      $('.header-wrp').css 'position', 'fixed'
      $('#tabs-fixed').css 'position', 'fixed'
      $('#footer-index').css 'display', 'block'
      return
  return
