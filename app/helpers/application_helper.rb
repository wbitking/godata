module ApplicationHelper
  def resource1_name
    :user
  end
  def resource1
    @resource1 ||= User.new
  end
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
