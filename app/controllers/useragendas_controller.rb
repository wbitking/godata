class UseragendasController < ApplicationController
  before_action :set_useragenda, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :attend]

  # GET /useragendas
  # GET /useragendas.json
  def index
    @useragendas = Useragenda.all
  end

  # GET /useragendas/1
  # GET /useragendas/1.json
  def show
  end

  # GET /useragendas/new
  def new
    @useragenda = Useragenda.new
  end

  # GET /useragendas/1/edit
  def edit
  end

  # POST /useragendas
  # POST /useragendas.json
  def create
    @useragenda = Useragenda.new(useragenda_params)

    respond_to do |format|
      if @useragenda.save
        format.html { redirect_to @useragenda, notice: 'Useragenda was successfully created.' }
        format.json { render :show, status: :created, location: @useragenda }
      else
        format.html { render :new }
        format.json { render json: @useragenda.errors, status: :unprocessable_entity }
      end
    end
  end

# GET /useragendas/attend/1
  def attend
      @agenda = Agenda.find(params[:id])
      if current_user.id==@agenda.sponsor_id
        #redirect_to @agenda, warning: '您是该邀约的发起者!'
        render :text => "您是该邀约的发起者!"
      else  
        if @agenda.isfull==1
          #redirect_to @agenda, warning: '人数已满!'
          render :text => "人数已满!"
        else
          current_user.useragendas.create(user_id:current_user.id, agenda_id:@agenda.id, isselected:0)
          #redirect_to @agenda, notice: '报名成功!'
          render :text => "报名成功!"
        end
      end
  end

  def select
    @agenda = Agenda.find(params[:id])
    
    if(@agenda.isoverdue==0)
      @t=@agenda.time
      @agendammt= @agenda.datatime.to_s+ " " +@t.strftime("%H")+":"+@t.strftime("%M")+":"+@t.strftime("%S")
      if((Time.parse(@agendammt)-Time.now)<=0)
        @agenda.update_attribute(:isoverdue, 1)
      end
    end

    if @agenda.isfull==1 || @agenda.isoverdue==1
      if @agenda.isfull==1
        redirect_to @agenda, warning: '人数已满!'
      else
        redirect_to @agenda, warning: '该邀约已过期!'
      end
    elsif params[:selected_ids]
          params[:selected_ids].each { |selected_id|
            @useragenda = Useragenda.where(user_id: selected_id, agenda_id: @agenda.id)
            @useragenda.update_all(:isselected => 1)
            # the count should exclude sponser itself
            if Useragenda.where(agenda_id:@agenda.id, isselected:1).count >= @agenda.userlimit
              @agenda.update_attribute(:isfull, 1)
              break
            end
          }
          if @agenda.isfull==1
            redirect_to @agenda, notice: 'this agenda attendees is full.'
          else  
            redirect_to @agenda
          end
    else
      redirect_to @agenda, notice: 'plese select person to attend.'
    end
  end

  # PATCH/PUT /useragendas/1
  # PATCH/PUT /useragendas/1.json
  def update
    respond_to do |format|
      if @useragenda.update(useragenda_params)
        format.html { redirect_to @useragenda, notice: 'Useragenda was successfully updated.' }
        format.json { render :show, status: :ok, location: @useragenda }
      else
        format.html { render :edit }
        format.json { render json: @useragenda.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /useragendas/1
  # DELETE /useragendas/1.json
  def destroy
    @useragenda.destroy
    respond_to do |format|
      format.html { redirect_to useragendas_url, notice: 'Useragenda was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_useragenda
      @useragenda = Useragenda.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def useragenda_params
      params.require(:useragenda).permit(:user_id, :agenda_id, :agendaFlag, :sponsorFlag)
    end
end
