class Api::V1::UptokenGenController < Api::V1::BaseController
  def index
    @put_policy = Qiniu::Auth::PutPolicy.new(
      'wxjqiniu',     # 存储空间
      nil,        # 最终资源名，可省略，即缺省为“创建”语义
      nil, # 相对有效期，可省略，缺省为3600秒后 uptoken 过期
      nil    # 绝对有效期，可省略，指明 uptoken 过期期限（绝对值），通常用于调试

    )

  @put_policy.save_key = "qiniuexample"
  @put_policy.mime_limit = "image/*;video/*"

  @token = Qiniu::Auth.generate_uptoken(@put_policy)

  render :json => {:uptoken => @token} 

  end
end
