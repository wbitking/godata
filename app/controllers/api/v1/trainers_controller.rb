class Api::V1::TrainersController < Api::V1::BaseController
  def show
    @trainer = Trainer.find(params[:id])
  end
end