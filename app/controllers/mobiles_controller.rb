class MobilesController < ApplicationController
  #before_action :set_mobile, only: [:show, :edit, :update, :destroy]

  # GET /mobiles
  # GET /mobiles.json
  def index
    @mobiles = Mobile.all
  end

  # GET /mobiles/1
  # GET /mobiles/1.json
  def show
  end

  # GET /mobiles/new
  def new
    @mobile = Mobile.new
  end

  # GET /mobiles/1/edit
  def edit
  end

  # POST /mobiles
  # POST /mobiles.json
  def create
    @mobile = Mobile.new(mobile_params)

    respond_to do |format|
      if @mobile.save
        format.html { redirect_to edit_mobile_path(@mobile), notice: 'Mobile was successfully created.' }
        format.json { render :show, status: :created, location: @mobile }
      else
        format.html { render :new }
        format.json { render json: @mobile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mobiles/1
  # PATCH/PUT /mobiles/1.json
  def update
    respond_to do |format|
      if @mobile.update(mobile_params)
        format.html { redirect_to @mobile, notice: 'Mobile was successfully updated.' }
        format.json { render :show, status: :ok, location: @mobile }
      else
        format.html { render :edit }
        format.json { render json: @mobile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mobiles/1
  # DELETE /mobiles/1.json
  def destroy
    @mobile.destroy
    respond_to do |format|
      format.html { redirect_to mobiles_url, notice: 'Mobile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_code
    @mobile = Mobile.new
    #mobile = Mobile.find(params[:id])
    @mobile.number = params[:number]
    #@mobile.verification_code = 1_000_000 + rand(10_000_000 - 1_000_000)
    @mobile.verification_code = 123456
    @mobile.is_verified=0
    if @mobile.save
      to = @mobile.number
      if to[0] = "0"
        to.sub!("0", '+86')
      end

      ChinaSMS.use :yunpian, password: ENV["yunpian_key"]
      ChinaSMS.to to, '【昱全科技】您的验证码是' + @mobile.verification_code

      render :text => "验证码已发送!"
      #redirect_to edit_mobile_path, :flash => { :success => "A verification code has been sent to your mobile. Please fill it in below." }
    else
      render :text => "手机号码有误或已注册!"
    end
  end

  def verify
    @mobile = Mobile.find_by(number: params[:number])

    if @mobile.verification_code == params[:code]
      @mobile.is_verified = 1
      @mobile.verification_code = ''
      @mobile.save
      #render :text => "验证成功!"
      render :json => @mobile.to_json
      #redirect_to new_user_registration_path(:mobile_id =>@mobile.id), :flash => { :success => "Thank you for verifying your mobile number." }
    else
      #redirect_to edit_mobile_path(@mobile), :flash => { :errors => "Invalid verification code." }
      render :text => "验证码错误!"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mobile
      @mobile = Mobile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mobile_params
      params.require(:mobile).permit(:number, :verification_code, :is_verified)
    end
end
