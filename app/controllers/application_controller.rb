class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  add_flash_types :success, :warning, :danger, :inf
  before_action :prepare_for_mobile
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :generate_signature
  before_action :set_userlog_status

  @@access_token = ""
  @@jsapi_ticket = ""
  @@timestamp_ticket = 0

protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :gender_id, :introduction, :icon, :carrer_id, :age_id, :birth, :email])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :gender_id, :introduction, :icon, :carrer_id, :age_id, :birth, :email])
  end

private

  def generate_signature
    @timestamp = Time.now.to_i
    #if @@jsapi_ticket == ""
    if @timestamp - @@timestamp_ticket >= 7200
      request = Typhoeus::Request.new("https://api.weixin.qq.com/cgi-bin/token", method: :get, params: {grant_type: "client_credential", appid: ENV["AppID"], secret: ENV["AppSecret"]})
      request.run
      response_json=JSON.parse(request.response.options[:response_body])
      @@access_token = response_json["access_token"]

      request = Typhoeus::Request.new("https://api.weixin.qq.com/cgi-bin/ticket/getticket", params: {access_token: @@access_token, type: "jsapi"})
      request.run
      response_json=JSON.parse(request.response.options[:response_body])
      @@jsapi_ticket = response_json["ticket"]

      @@timestamp_ticket = @timestamp
    end

    @noncestr = SecureRandom.base64(16)
    #@timestamp = Time.now.to_i

    params = {
      noncestr: @noncestr,
      timestamp: @timestamp,
      jsapi_ticket: @@jsapi_ticket,
      url: "http://www.wejeek.com/setloc"
      #url: "http://www.wejeek.com/"
    }
    pairs = params.keys.sort.map do |key|
      "#{key}=#{params[key]}"
    end
    @signature = Digest::SHA1.hexdigest pairs.join('&')

  end

  def mobile_device?
    request.user_agent =~ /Mobile|webOS/
  end
  helper_method :mobile_device?

  def prepare_for_mobile
    request.format = :mobile if mobile_device?
  end

  def set_userlog_status
    cookies[:login] = user_signed_in?
  end

end
