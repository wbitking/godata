class Users::RegistrationsController < Devise::RegistrationsController
# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]
  before_filter :resource, only: [:home, :album, :info, :attends, :pubs, :settings, :icon, :nick, :gender, :age]
  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    super
    
    current_user.verify_code = 123456
    current_user.is_verified=0
    if current_user.save
      to = current_user.phone
      if to[0] == "0"
        to.sub!("0", '+86')
      end
      #ChinaSMS.use :yunpian, password: ENV["yunpian_key"]
      #ChinaSMS.to to, '【昱全科技】您的验证码是' + @mobile.verification_code
      #redirect_to edit_mobile_path, :flash => { :success => "A verification code has been sent to your mobile. Please fill it in below." }
      #render :text => "codesent"
    else
      #render :text => "codenotsent"
    end

    sign_out current_user
    #resource.save
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.for(:sign_up) << :attribute
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
    #edit_user_registration_path(resource)
    '/user/verify_code?phone='+resource.phone
    #sign_out current_user
  end

  def after_update_path_for(resource)
    if mobile_device?
      '/info'
    else
      '/home'
    end
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end


  # routes for user settings
  
  def profile
      @user = User.find(params[:id])
      @pictures = @user.pictures
  end

  def home
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :userhome
    else
      redirect_to '/users/sign_in'
    end
  end

  def album
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :useralbum
    else
      redirect_to '/users/sign_in'
    end
  end

  def info
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :userinfo
    else
      redirect_to '/users/sign_in'
    end
  end

  def attends
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :userattends
    else
      redirect_to '/users/sign_in'
    end
  end

  def pubs
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :userpubs
    else
      redirect_to '/users/sign_in'
    end
  end

  def settings
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :usersettings
    else
      redirect_to '/users/sign_in'
    end
  end

  def nick
    if user_signed_in?
      @pictures = resource.pictures
      @sponser_agendas = Agenda.all.where(sponsor_id: resource.id).order("created_at DESC")
      @attend_agendas = resource.agendas - @sponser_agendas
      render :userinfonick
    else
      redirect_to '/users/sign_in'
    end
  end


private
  
  def resource
    if user_signed_in?
      current_user
    else
      #redirect_to '/users/sign_in'
      # for sign up
      instance_variable_get(:"@#{resource_name}")
    end
  end

end
