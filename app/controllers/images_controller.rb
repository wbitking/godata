class ImagesController < ApplicationController

  # POST /images
  # POST /images.json
  def create
  	@agenda = Agenda.find(params[:agenda_id])
    respond_to do |format|
      if params[:images]
          #===== The magic is here ;)
          params[:images].each { |image|
            @agenda.images.create(image: image, agenda_id: @agenda.id)
          }
      end
        format.mobile { redirect_to @agenda, notice: 'image was successfully created.' }
        format.html { redirect_to @agenda, notice: 'image was successfully created.' }
        format.json { render :show, status: :created, location: @agenda }
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
  	@image = Image.find(params[:id])
  	@agenda = Agenda.find(@image.agenda_id)
    @image.destroy
    respond_to do |format|
      format.mobile { redirect_to @agenda, notice: 'image was successfully destroyed.' }
      format.html { redirect_to @agenda, notice: 'image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


end
