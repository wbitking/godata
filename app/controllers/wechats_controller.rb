require 'digest/sha1'

require 'typhoeus'

class WechatsController < ApplicationController

  skip_before_action :verify_authenticity_token, only: :process_request

  @@token="201608292048sh"
  def auth_wechat
    if check_signature?(params[:signature],params[:timestamp],params[:nonce])
     return render text: params[:echostr]
    end
  end

  def process_request
    if check_signature?(params[:signature], params[:timestamp], params[:nonce]) #验证消息真实性
      if params[:xml][:MsgType] == "event"
        if params[:xml][:Event] == "subscribe"
          render "info", layout: false, :formats => :xml          #用户加关注的时候
        end
      elsif (params[:xml][:MsgType]=="text")&&(params[:xml][:Content]=="create nav")
          create_nav
          render "info", layout: false, :formats => :xml
      else
        render "info", layout: false, :formats => :xml  #用户输入消息时，回送欢迎关注
      end
    end
  end

  private
    def check_signature?(signature,timestamp,nonce)
      Digest::SHA1.hexdigest([timestamp,nonce,@@token].sort.join) == signature
    end

    def create_nav
      response=Typhoeus.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxe133a9303b640322&secret=5e9b96aeb48715e1c88142d4b78347ad")
      response_json=JSON.parse(response.options[:response_body])
      get_access_token = response_json["access_token"]

      post_url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=#{get_access_token}"
      nav_yml = load_yml_file "nav.yml"
      post_hash = nav_yml['menu'].to_json
      Typhoeus.post(post_url, body: post_hash)
    end

    def load_yml_file file_name
      yml_name = Rails.root.join(Rails.root, 'app/views/wechats', file_name)
      YAML.load_file(yml_name)
    end

    #处理菜单中文问题  
    # def generate_post_hash post_hash
    #   post_hash.to_json.gsub!(/\\u([0-9a-z]{4})/) { |s| [$1.to_i(16)].pack("U") }
    # end

    # def get_access_token
    #   response=Typhoeus.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxe133a9303b640322&secret=5e9b96aeb48715e1c88142d4b78347ad")
    #   response_json=JSON.parse(response.options[:response_body])
    #   response_json["access_token"]
    # end
end