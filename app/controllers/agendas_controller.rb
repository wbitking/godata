class AgendasController < ApplicationController
  before_action :set_agenda, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

  # GET /agendas
  # GET /agendas.json
  def index
    if params[:zone_id]
      @zone = Zone.find(params[:zone_id])
      redirect_to @zone
    elsif params[:category_id]
      @category = Category.find(params[:category_id])
      redirect_to @category
    elsif params[:query]
      #@agendas_searched = Agenda.text_search(params[:query])
      @agendas = Agenda.text_search(params[:query]).order(:isoverdue, :datatime).paginate(page: params[:page], per_page: 12)
      @categories = Category.all.order(:id);
      @agendas.each do |agenda|
        if(agenda.isoverdue==0)
          @t=agenda.time
          @agendammt= agenda.datatime.to_s+ " " +@t.strftime("%H")+":"+@t.strftime("%M")+":"+@t.strftime("%S")
          if((Time.parse(@agendammt)-Time.now)<=0)
            agenda.update_attribute(:isoverdue, 1)
          end
        end
      end
      @agendas_for_index = Agenda.all
      render :search
    else
      @agendas = Agenda.all.order(:isoverdue, :datatime).paginate(page: params[:page], per_page: 12)
      @categories = Category.all.order(:id);
      @agendas.each do |agenda|
        if(agenda.isoverdue==0)
          @t=agenda.time
          @agendammt= agenda.datatime.to_s+ " " +@t.strftime("%H")+":"+@t.strftime("%M")+":"+@t.strftime("%S")
          if((Time.parse(@agendammt)-Time.now)<=0)
            agenda.update_attribute(:isoverdue, 1)
          end
        end
      end
      @agendas_for_index = Agenda.all
    end
  end

  # GET /agendas/1
  # GET /agendas/1.json
  def show
    @t=@agenda.time
    @agendammt= @agenda.datatime.to_s+ " " +@t.strftime("%H")+":"+@t.strftime("%M")+":"+@t.strftime("%S")
    if(@agenda.isoverdue==0)
      if((Time.parse(@agendammt)-Time.now)<=0)
        @agenda.update_attribute(:isoverdue, 1)
      end
    end
    @messages = @agenda.messages
  end

  # GET /agendas/new
  def new
    @agenda = Agenda.new
  end

  # GET /agendas/1/edit
  def edit
  end

  # POST /agendas
  # POST /agendas.json
  def create
    @agenda = Agenda.new(agenda_params)
    @agenda.sponsor_id = current_user.id
    @agenda.isoverdue = 0
    @agenda.isfull = 0

    respond_to do |format|
      if @agenda.save
        if params[:images]
          #===== The magic is here ;)
          params[:images].each { |image|
            @agenda.images.create(image: image, agenda_id:@agenda.id)
          }
        else
          @agenda.images.create(image: current_user.icon, agenda_id:@agenda.id)  
        end
        current_user.useragendas.create(user_id:current_user.id, agenda_id:@agenda.id, isselected:2)
        format.mobile { redirect_to @agenda, notice: '发布成功!' }
        format.html { redirect_to @agenda, notice: '发布成功!' }
        format.json { render :show, status: :created, location: @agenda }
      else
        format.mobile { render :new }
        format.html { render :new }
        format.json { render json: @agenda.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /agendas/1
  # PATCH/PUT /agendas/1.json
  def update
    @agenda.isoverdue = 0
    @agenda.isfull = 0
    respond_to do |format|
      if @agenda.update(agenda_params)

        if params[:images]
          #===== The magic is here ;)
          params[:images].each { |image|
            @agenda.images.first.update_attribute(:image, image)
          }
        else
          #@agenda.images.create(image: current_user.icon, agenda_id:@agenda.id)  
        end
        format.mobile { redirect_to @agenda, notice: '成功更新邀约!' }
        format.html { redirect_to @agenda, notice: '成功更新邀约!' }
        format.json { render :show, status: :ok, location: @agenda }
      else
        format.mobile { render :edit }
        format.html { render :edit }
        format.json { render json: @agenda.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /agendas/1
  # DELETE /agendas/1.json
  def destroy
    @agenda.destroy
    respond_to do |format|
      format.mobile { redirect_to '/home', notice: '成功删除邀约!' }
      format.html { redirect_to '/home', notice: '成功删除邀约!' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agenda
      @agenda = Agenda.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agenda_params
      params.require(:agenda).permit(:name, :description, :category_id, :province, :city, :zone_id, :cost_id, :address, :lng, :lat, :datatime, :time, :userlimit, :images)
    end
end
