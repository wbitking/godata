class UsersController < ApplicationController

  #before_action :authenticate_user!

  def edit
    @user = current_user
  end

  def forget_password
    #render :forget_password
  end

  def reset_password
    #render :reset_password
  end

  def create_user
    #render :reset_password
  end
 
  def verify_phone
    @user = User.find_by(phone: params[:number])
    if @user != nil
      if @user.is_verified!=1
        render :text => "existnotverified"
      else
        render :text => "existedverified"
      end
    else
      render :text => "newphone"
    end
  end

  def verify_code
    render :verify_code
  end

  def create_code
    @user = User.find_by(phone: params[:number])
    if params[:isregister]=='1'
      if @user != nil
        if @user.is_verified!=1
          #@mobile.verification_code = 1_000_000 + rand(10_000_000 - 1_000_000)
          @user.verify_code = 123456
          @user.is_verified=0
          if @user.save
            to = @user.phone
            if to[0] == "0"
              to.sub!("0", '+86')
            end
            #ChinaSMS.use :yunpian, password: ENV["yunpian_key"]
            #ChinaSMS.to to, '【昱全科技】您的验证码是' + @mobile.verification_code
            #redirect_to edit_mobile_path, :flash => { :success => "A verification code has been sent to your mobile. Please fill it in below." }
            render :text => "codesent"
          else
            render :text => "codenotsent"
          end
        else
          render :text => "exist"
        end
      else

      end
    else # forget password
      if @user != nil
        @user.verify_code = 123456
        @user.is_verified=0
        if @user.save
          to = @user.phone
          if to[0] == "0"
            to.sub!("0", '+86')
          end
          #ChinaSMS.use :yunpian, password: ENV["yunpian_key"]
          #ChinaSMS.to to, '【昱全科技】您的验证码是' + @mobile.verification_code
          #redirect_to edit_mobile_path, :flash => { :success => "A verification code has been sent to your mobile. Please fill it in below." }
          render :text => "codesent"
        else
          render :text => "codenotsent"
        end
      else
        render :text => "notexist"
      end  
    end

  end


  def code_verification
    @user = User.find_by(phone: params[:number])

    if @user.verify_code == params[:code]
      @user.is_verified = 1
      @user.verify_code = ''
      @user.save
      sign_in @user
      render :text => "verify_pass"
    else
       render :text => "verify_fail!"
    end

  end


  def update_password
    @user = User.find(params[:userid])
    if @user.update(user_params)
      # Sign in the user by passing validation in case their password changed
      sign_in @user, :bypass => true
      redirect_to root_path
    else
      render "edit"
    end
  end

  private

  def user_params
    # NOTE: Using `strong_parameters` gem
    params.require(:user).permit(:password, :password_confirmation)
  end
end