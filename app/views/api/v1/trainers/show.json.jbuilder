json.trainer do
  json.(@trainer, :id, :email, :name,  :activated, :admin, :created_at, :updated_at)
end