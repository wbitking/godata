json.array!(@agendas) do |agenda|
  json.extract! agenda, :id, :name, :description, :province, :city, :address, :datatime
  json.url agenda_url(agenda, format: :json)
end
