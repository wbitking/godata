json.array!(@useragendas) do |useragenda|
  json.extract! useragenda, :id, :user_id, :agenda_id, :agendaFlag, :sponsorFlag
  json.url useragenda_url(useragenda, format: :json)
end
