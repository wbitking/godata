json.array!(@reps) do |rep|
  json.extract! rep, :id, :comment
  json.url rep_url(rep, format: :json)
end
