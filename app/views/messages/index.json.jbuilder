json.array!(@messages) do |message|
  json.extract! message, :id, :comment
  json.url message_url(message, format: :json)
end
