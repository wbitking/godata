class Rep < ActiveRecord::Base
	validates :comment, presence: true
	belongs_to :message
end
