class Message < ActiveRecord::Base
	validates :comment, presence: true
	belongs_to :agenda
	has_many :reps, dependent: :destroy
end
