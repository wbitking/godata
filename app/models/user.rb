class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :icon, UserUploader

  has_many :useragendas
  has_many :agendas, through: :useragendas
  has_many :pictures

  belongs_to :age
  belongs_to :carrer
  belongs_to :gender

  #validates :name, :introduction, :icon, :gender_id, :age_id, :carrer_id, presence: true
  def email_required?
    false
  end
 
  def email_changed?
    false
  end
end
