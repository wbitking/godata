class Agenda < ActiveRecord::Base


	has_many :useragendas, dependent: :destroy 
	has_many :users, through: :useragendas
	has_many :images, dependent: :destroy
	has_many :messages, dependent: :destroy
	
    belongs_to :category
    belongs_to :zone
    belongs_to :cost

    validates :name, :category_id, :zone_id, :time, :datatime, presence: true
    validates :userlimit, numericality: {greater_or_equal_than: 1, less_or_equal_than: 100}

    def self.text_search(query)
      if query.present?
      	where("name ilike :q or description ilike :q", q: "%#{query}%")
      else
      	scoped
      end 
    end 
end
